# Seed Color Picker Demo

The goal of this example was to port the
[Elm Nested Reusable View Functions](https://medium.com/@mickey.vip/an-approach-to-nested-reusable-view-functions-in-elm-a1531b9abaf3)
To Seed, framework for creating web applications that uses [Rust](https://rust-lang.org)
to compile to [WebAssembly](https://webassembly.org/).

Each commit in the repo roughly corresponds to one step in the tutorial.

Visit https://mkroehnert.gitlab.io/seed-color-picker-example/ in order to see the demo.

# Building and Running Locally

First, you need to install `wasm-pack` and any webserver that serves WebAssembly modules correctly.
I'll be using `microserver`

```sh
$ cargo install wasm-pack microserver
```

Then clone the repository (get the URL from the
[Gitlab page](https://gitlab.com/mkroehnert/seed-color-picker-example))
and run the following commands in the checked out directory:

```sh
$ wasm-bindgen build --target web
$ microserver
```

Afterwards visit the URL [http://127.0.0.1:9090/](http://127.0.0.1:9090/)
to see the demo page.

NOTE: you will need a recent version of Firefox or any browser that supports native ES6 modules to run the demo.

# Used Open Source Libraries #

* [wasm-bindgen](https://rustwasm.github.io/docs/wasm-bindgen/)
* [Seed (Rust WebAssembly framework)](https://seed-rs.org)

# License

The source code of this Seed  demo is available under the MIT license.
See [LICENSE](LICENSE) for details.
