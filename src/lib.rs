// SPDX-License-Identifier: MIT

use seed::prelude::*;
use seed::*;

type ColorChannelType = u8;

#[derive(Debug, Copy, Clone, Default)]
pub struct Color {
    red: ColorChannelType,
    green: ColorChannelType,
    blue: ColorChannelType,
}

fn clone_with_red(color: Color, new_red: ColorChannelType) -> Color {
    Color{
        red: new_red,
        green: color.green,
        blue: color.blue,
    }
}
fn clone_with_green(color: Color, new_green: ColorChannelType) -> Color {
    Color{
        red: color.red,
        green: new_green,
        blue: color.blue,
    }
}
fn clone_with_blue(color: Color, new_blue: ColorChannelType) -> Color {
    Color{
        red: color.red,
        green: color.green,
        blue: new_blue,
    }
}

fn to_color_css(color: Color) -> String
{
    format!("rgb({},{},{})", color.red, color.green, color.blue)
}


fn to<T>(string: &str, default: T) -> T
    where T: std::cmp::PartialEq + std::str::FromStr
{
    string.parse::<T>().unwrap_or(default)
}

fn color_slider<M>(name: &str, color: ColorChannelType, to_msg: impl FnOnce(ColorChannelType) -> M + 'static + Clone) -> Node<M>
{
    div![
        p![name],
        input![
            attrs![At::Type => "range",
                   At::Name => format!("color-{}", name),
                   At::Min => "0",
                   At::Max => "255",
                   At::Value => color,
            ],
            input_ev(Ev::Input, |input| to_msg(to::<ColorChannelType>(&input, 0))),
        ],
        span![
            color.to_string(),
        ],
    ]
}


fn color_picker<M>(color: Color, to_msg: impl FnOnce(Color) -> M + 'static + Clone) -> Node<M> {
    div![
        color_slider(
            "Red",
            color.red,
            {
                let to_msg = to_msg.clone();
                move |red_value| to_msg(clone_with_red(color, red_value))
            },
        ),
        color_slider(
            "Green",
            color.green,
            {
                let to_msg = to_msg.clone();
                move |green_value| to_msg(clone_with_green(color, green_value))
            },
        ),
        color_slider(
            "Blue",
            color.blue,
            {
                let to_msg = to_msg.clone();
                move |blue_value| to_msg(clone_with_blue(color, blue_value))
            },
        ),
        div![
            style!{
                St::Width => "100px",
                St::Height => "100px",
                St::BackgroundColor => to_color_css(color)},
        ],
    ]
}


#[derive(Default)]
struct Model {
    color1: Color,
    color2: Color,
}

#[derive(Debug, Clone)]
enum Message {
    UpdateColor1(Color),
    UpdateColor2(Color),
}

fn after_mount(_url: Url, _orders: &mut impl Orders<Message>) -> AfterMount<Model> {
    AfterMount::new(Model {
        color1: Color {
            red: 50,
            green: 200,
            blue: 100,
        },
        color2: Color {
            red: 0,
            green: 255,
            blue: 255,
        },
    }).url_handling(UrlHandling::None)
}

fn update(msg: Message, model: &mut Model, _: &mut impl Orders<Message>) {
    match msg {
        Message::UpdateColor1(new_color) => model.color1 = new_color,
        Message::UpdateColor2(new_color) => model.color2 = new_color,
    }
}

fn view(model: &Model) -> impl View<Message> {
    div![
        h1![
            style!{St::Color => to_color_css(model.color1)},
            "Color Picker 1",
        ],
        color_picker(model.color1, Message::UpdateColor1),
        hr![],
        h1![
            style!{St::Color => to_color_css(model.color2)},
            "Color Picker 2",
        ],
        color_picker(model.color2, Message::UpdateColor2),
    ]
}

#[wasm_bindgen(start)]
pub fn render() {
    seed::App::builder(update, view)
        .after_mount(after_mount)
        .build_and_start();
}
